import axios from "../utils/axios";

let dir = require.context("./config", true, /\.js$/);

let res = dir.keys().reduce((prev, item) => {
  let filename = item.match(/\/(\w+)\.js/)[1];
  prev[filename] = Object.keys(dir(item)).reduce((p, key) => {
    p[key] = (data = {}, params = {}) =>
      axios({ ...dir(item)[key], data, params });
    return p;
  }, {});
  return prev;
}, {});

export default res;
