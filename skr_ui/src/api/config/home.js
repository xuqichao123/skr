// 上面选项卡
export const selectStationLabel = {
  url: "/dev-api/sxpt/station/selectStationLabel",
  method: "get",
};

// 下面表格数据
export const selectStationNewVersionList = {
  url: "/dev-api/sxpt/station/selectStationVersionList",
  method: "get",
};
