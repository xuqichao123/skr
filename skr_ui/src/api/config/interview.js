export const _selectStationLabel = {
  url: "/dev-api/sxpt/station/selectStationLabel",
  method: "get",
};
export const _interviewList = {
  url: "/dev-api/sypt/interview/interviewList",
  method: "get",
};
export const _getClassInfo = {
  url: "/dev-api/sxpt/classPlan/getClassInfo",
  method: "get",
};
export const _interviewRecordRangkingTeacher = {
  url: "/dev-api/sxpt/interview/interviewRecordRangkingTeacher",
  method: "get",
};
export const _interviewAnswerRangkingTeacher = {
  url: "/dev-api/sxpt/interview/interviewAnswerRangkingTeacher",
  method: "get",
};
