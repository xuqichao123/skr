import {
  ElMessage,
  ElRadio,
  ElButton,
  ElTable,
  ElTableColumn,
  ElInput,
  ElMenu,
  ElMenuItem,
  ElSubmenu,
  ElIcon,
  ElBreadcrumb,
  ElBreadcrumbItem,
  ElRadioButton,
  ElRadioGroup,
  ElPagination,
  ElDropdown,
  ElDropdownMenu,
  ElDropdownItem,
  ElCheckbox,
  ElSelect,
  ElOption,
  ElProgress,
  ElTabs,
  ElTabPane,
} from "element-plus";

import NavData from "../compontents/Breadcrumb/breadcrumb.vue";

export default {
  install(vue) {
    vue.use(ElMessage);
    vue.use(ElRadio);
    vue.use(ElButton);
    vue.use(ElTable);
    vue.use(ElTableColumn);
    vue.use(ElInput);
    vue.use(ElMenu);
    vue.use(ElMenuItem);
    vue.use(ElSubmenu);
    vue.use(ElIcon);
    vue.use(ElBreadcrumb);
    vue.use(ElBreadcrumbItem);
    vue.use(ElRadioButton);
    vue.use(ElRadioGroup);
    vue.use(ElPagination);
    vue.use(ElDropdown);
    vue.use(ElDropdownMenu);
    vue.use(ElDropdownItem);
    vue.use(ElCheckbox);
    vue.use(ElSelect);
    vue.use(ElOption);
    vue.use(ElProgress);
    vue.use(ElTabs);
    vue.use(ElTabPane);
    vue.component("NavData", NavData);
  },
};
