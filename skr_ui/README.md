# 实训就业平台
修改之后得代码
# 使用

```
$ git clone git@gitlab.com:typeofYh/jiuye.git

$ cd jiuyepingtai

$ npm install

$ npm run serve

```

# git提交规范

> 1.提交格式（注意冒号后面有空格）
```
<type>: <subject>
```

> 2. 
upd：更新某功能（不是 feat, 不是 fix）

feat：新功能（feature）

fix：修补bug

docs：文档（documentation）

style： 格式（不影响代码运行的变动）

refactor：重构（即不是新增功能，也不是修改bug的代码变动）

test：增加测试

chore：构建过程或辅助工具的变动

> 3.例子：git commit -m "feat: 增加 xxx 功能"


# 项目规范依赖

"@commitlint/cli": 安装commitlint指令 作用检测commit -m 的提交信息

"@commitlint/config-conventional": commitlint指令的基础规范包

"@vue/eslint-config-prettier": 解决eslint和prettier兼容

"eslint-plugin-prettier": ellint的插件配合prettier使用

"husky": 封装了git hooks huskyrc pre-commit:lintstaged

"lint-staged": 检测整体项目文件  .lintstagedrc

"prettier": 规范代码风格