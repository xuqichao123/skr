const path = require("path");
function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/assets/Common/common.css")],
    });
}
module.exports = {
  lintOnSave: process.env.NODE_ENV === "development",
  devServer: {
    proxy: {
      "/dev-api": {
        target: "http://111.203.59.61:8060",
      },
    },
  },
  chainWebpack: (config) => {
    const types = ["vue-modules", "vue", "normal-modules", "normal"];
    types.forEach((type) =>
      addStyleResource(config.module.rule("scss").oneOf(type))
    );
  },
};
